# Correct bootanimation size for the screen
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

$(call inherit-product, device/elephone/p6000/full_p6000.mk)

PRODUCT_NAME := cm_p6000
PRODUCT_DEVICE :=p6000
PRODUCT_BRAND := elephone
PRODUCT_MANUFACTURER := Elephone
PRODUCT_MODEL := P6000
